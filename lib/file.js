'use strict';
var path = require('path');
var fs = require('fs');
var parse5 = require('parse5');

// File is responsible to gather all information related to a given parsed file, as:
//  - its dir and name
//  - its content
//  - the search paths where referenced resource will be looked at
//  - the list of parsed blocks
//
//
// Returns an array object of all the directives for the given html.
// Each item of the array has the following form:
//
//
//     {
//       type: 'css',
//       dest: 'css/site.css',
//       src: [
//         'css/normalize.css',
//         'css/main.css'
//       ],
//       raw: [
//         '    <!-- build:css css/site.css -->',
//         '    <link rel="stylesheet" href="css/normalize.css">',
//         '    <link rel="stylesheet" href="css/main.css">',
//         '    <!-- endbuild -->'
//       ]
//     }
//
// Note also that dest is expressed relatively from the root. I.e., if the block starts with:
//    <!-- build:css /foo/css/site.css -->
// then dest will equal foo/css/site.css (note missing trailing /)
//
var getAttributes = function (attributes) {
  var tmp = {};
  return attributes.reduce(function (previousValue, currentValue) {
    tmp[currentValue.name] = currentValue.value;
    return tmp;
  }, tmp);
};

var getBlocks = function (content) {
  // start build pattern: will match
  //  * <!-- build:[target] output -->
  //  * <!-- build:[target](alternate search path) output -->
  // The following matching param are set when there's a match
  //   * 0 : the whole matched expression
  //   * 1 : the target (i.e. type)
  //   * 2 : the alternate search path
  //   * 3 : the output
  //
  var regbuild = /\s*build:(\w+)(?:\(([^\)]+)\))?\s*([^\s]+)\s*/;
  // end build pattern -- <!-- endbuild -->
  var regend = /\s*endbuild\s*/;
  var block = false;
  var sections = [];
  var last;
  var startFromRoot = false;
  var asset = null;

  var parser = new parse5.SimpleApiParser({
    startTag: function (tagName, attrs, selfClosing , location) {
      if ((tagName === 'link') && block && last) {
        var attributes = getAttributes(attrs);
        if (attributes) {
           if (attributes.href && !!attributes.href) {
            asset = attributes.href;
          }
        }

        if (asset) {
          last.src.push(asset);

          // FIXME: media attribute should be present for all members of the block *and* having the same value
          if (attributes && attributes.media && !!attributes.media) {
            last.media = attributes.media;
          }

          // RequireJS uses a data-main attribute on the script tag to tell it
          // to load up the main entry point of the amp app
          //
          // If we find one, we must record the name of the main entry point,
          // as well the name of the destination file, and treat
          // the furnished requirejs as an asset (src)
          var main = attributes && attributes['data-main'];
          if (main) {
            throw new Error('require.js blocks are no more supported.');
          }
        }

        last.raw.push(content.substring(location.start, location.end));
        asset = null;
      }
    },
    comment: function (text, location) {
      var indent = (text.match(/^\s*/) || [])[0];
      var endbuild = regend.test(text);
      var build = text.match(regbuild);

      // discard empty lines
      if (build && build[1] === "css") {
        block = true;
        last = {
          type: build[1],
          dest: build[3],
          startFromRoot: startFromRoot,
          indent: indent,
          searchPath: [],
          src: [],
          raw: [content.substring(location.start, location.end)]
        };

        if (build[2]) {
          // Alternate search path
          last.searchPath.push(build[2]);
        }
      }

      // Check IE conditionals
      var isConditionalStart = text.match(/(\[if.*\]>)(<!-->)?( -->)?/g);
      var isConditionalEnd = text.match(/(<!--\s?)?(<!\[endif\])/g);
      if (block && isConditionalStart) {
        last.conditionalStart = isConditionalStart;
      }
      if (block && isConditionalEnd) {
        last.conditionalEnd = isConditionalEnd;
      }

      // switch back block flag when endbuild
      if (block && endbuild) {
        last.raw.push(content.substring(location.start, location.end));
        sections.push(last);
        block = false;
      }
    }
  }, {
    decodeEntities: true,
    locationInfo: true
  });

  parser.parse(content);

  return sections;
};


module.exports = function (filepath) {
  this.dir = path.dirname(filepath);
  this.name = path.basename(filepath);
  // By default referenced content will be looked at relative to the location
  // of the file
  this.searchPath = [this.dir];
  this.content = fs.readFileSync(filepath).toString();

  // Let's parse !!!
  this.blocks = getBlocks(this.content);
};
