'use strict';
var debug = require('debug')('fileprocessor');
var escapeStringRegexp = require('escape-string-regexp');
var File = require('./file');
var _ = require('lodash');

//
// Default block replacement functions, for css and js types
//
var defaultBlockReplacements = {
  css: function (block) {
    var media = block.media ? ' media="' + block.media + '"' : '';
    return '<link rel="stylesheet" href="' + block.dest + '"' + media + '>';
  },
  js: function (block) {
    var defer = block.defer ? 'defer ' : '';
    var async = block.async ? 'async ' : '';
    return '<script ' + defer + async + 'src="' + block.dest + '"><\/script>';
  }
};

var FileProcessor = module.exports = function (filename) {
	this.filename = filename;
	this.blockReplacements = _.assign({}, defaultBlockReplacements);
};

//
// Replace blocks by their target
//
FileProcessor.prototype.replaceBlocks = function replaceBlocks(file, keep) {
  var result = file.content;

  file.blocks.forEach(function (block) {
	var firstIndex = 0;
	var lastIndex = block.raw.length - 1;
	if(keep) {
		firstIndex = firstIndex + 1;
		lastIndex = lastIndex - 1;
	}
    var firstBlock = escapeStringRegexp(block.raw[firstIndex] + '');
    var lastBlock = escapeStringRegexp(block.raw[lastIndex] + '');

    var expression = (firstBlock + '[\\s\\S]*?' + lastBlock);

    var regex = new RegExp(expression, 'm');

    result = result.replace(regex, this.replaceWith(block));
  }, this);
  return result;
};


FileProcessor.prototype.replaceWith = function replaceWith(block) {
  var result;
  var conditionalStart = block.conditionalStart ? block.conditionalStart + '\n' + block.indent : '';
  var conditionalEnd = block.conditionalEnd ? '\n' + block.indent + block.conditionalEnd : '';
  if (typeof block.src === 'undefined' || block.src === null || block.src.length === 0) {
    // there are no css or js files in the block, remove it completely
    result = '';
  } else if (_.includes(_.keys(this.blockReplacements), block.type)) {
    result = block.indent + conditionalStart + this.blockReplacements[block.type](block) + conditionalEnd;
  } else {
    result = '';
  }
  return result;
};

FileProcessor.prototype._filenameToFile = function () {
  if (_.isString(this.filename)) {
    this.file = new File(this.filename);
  } else {
    // filename is an object and should conform to lib/file.js API
    this.file = this.filename;
  }
};

FileProcessor.prototype._resultstructure = function () {
  var result = {filename: this.filename, css: []};
  this.file.blocks.forEach(function(block) {
		result.css.push(block);
  });
  return result;
};

FileProcessor.prototype.process = function (keep) {
  debug('processing file %s', this.filename);
  this._filenameToFile();

  var result = this._resultstructure();
  result.content = this.replaceBlocks(this.file, keep);
  return result;
};

FileProcessor.prototype.parse = function () {
  debug('parsing file %s', this.filename);
  this._filenameToFile();

  return this._resultstructure();
};
