'use strict';

var path = require('path');
var longestCommonRootpath = require("./longest-common-rootpath.js");

module.exports = function(cwd, files) {
	var result = cwd+path.sep+longestCommonRootpath(files);
	return path.normalize(result);
};

