var path = require('path');

module.exports = function(files) {
	if(!files || files && files.length <= 0) {
		return '';
	}

	var result = "";
	var fileParts = files.map(function(file) {
		return path.dirname(file).split(path.sep);
	});
	fileParts = transpose(fileParts);
	fileParts.every(function(column) {
		var allSame = allValuesEqual(column);
		if(allSame) {
			result = result+column[0]+path.sep;
		}
		return allSame;
	});

	return result;
};

function transpose(array) {
	return Object.keys(array[0])
		.map(function (column) {
			return array.map(function (row) {
				return row[column];
			});
		});
}

function allValuesEqual(array) {
	return array.every(function(pathPart) {
		return pathPart === array[0];
	});
}

