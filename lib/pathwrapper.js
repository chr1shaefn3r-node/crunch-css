'use strict';
var path = require('path'),
	pathIsAbsolute = require('path-is-absolute'),
	htmlRoot = require('./htmlroot.js');

module.exports = function(cwd) {
	var htmlroot = function(files) {
		var cwdWithTrailingSlash = cwd+path.sep;
		var hr = htmlRoot(cwd, files);
		if(cwdWithTrailingSlash === hr ) {
			return undefined;
		}
		return hr;
	};
	var wrapCss = function(filePath, cssPath, htmlroot) {
		if(htmlroot) {
			return path.normalize(htmlroot+cssPath);
		}
		var completePath = path.dirname(path.join(cwd, filePath));
		var sep = "";
		if(!pathIsAbsolute(cssPath)) {
			sep = path.sep;
		}
		return completePath+sep+cssPath;
	};
	var wrap = function(filePath) {
		if(startsWith(filePath, cwd)) {
			return filePath;
		}
		var sep = "";
		if(!pathIsAbsolute(filePath)) {
			sep = path.sep;
		}
		return cwd+sep+filePath;
	};
	var shorten = function(completePath, newRoot) {
		var startPosition = 0;
		if(newRoot) {
			startPosition = completePath.search(newRoot) + newRoot.length-1;
		} else {
			startPosition = cwd.length + 1;
		}
		return completePath.slice(startPosition, completePath.length);
	};

	function startsWith(base, searchString) {
		var position = position || 0;
		return base.indexOf(searchString, position) === position;
	}

	return Object.freeze({
		htmlroot: htmlroot,
		wrapCss: wrapCss,
		wrap: wrap,
		shorten:shorten
	});
};

