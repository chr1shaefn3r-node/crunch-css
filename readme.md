# crunch-css

> Generates common css file for all passed html-pages and\nrewrites them to include that css-file (via usemin blocks).

## CLI

```sh
$ npm install --global crunch-css
```

```sh
$ crunch-css --help

  Usage:
    crunch-css [options] FILES

  Options:
    -p, --projectroot  your htmlroot
    -k, --keep         keep usemin blocks
    -h, --help         print usage information
    -v, --version      show version info and exit

  Examples:
    $ crunch-css index.html

    Use bash globbing:
    $ crunch-css *.html
```
### Tripping Hazard
If your webproject uses relative paths to css-files, you *need* to invoke the tool from your htmlroot folder, otherwise uncss will fail to find your stylesheets.

## Usemin
Check out the [Usemin-Readme](https://www.npmjs.com/package/grunt-usemin) to learn how to add blocks to your html-files.


## License

MIT © [Christoph Häfner](http://christophhaefner.de)

