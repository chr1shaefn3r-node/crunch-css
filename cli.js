#!/usr/bin/env node
'use strict';
var fs = require('fs');
var meow = require('meow'),
	chalk = require('chalk');
var humanSize = require('human-size'),
	percentage = require("percentage");
var PathWrapper = require('./lib/pathwrapper.js'),
	pathWrapper = new PathWrapper(process.cwd()),
	shorten = pathWrapper.shorten,
	wrap = pathWrapper.wrap;
var crunchCss = require('./');

var cli = meow({
	help: [
		'Usage:',
		'  crunch-css [options] FILES',
		'',
		'Options:',
		'  -p, --projectroot  your htmlroot',
		'  -k, --keep         keep usemin blocks',
		'  -h, --help         print usage information',
		'  -v, --version      show version info and exit',
		'',
		'Examples:',
		'  $ crunch-css index.html',
		'',
		'  Use bash globbing:',
		'  $ crunch-css *.html',
	].join('\n')
},{
	alias: { h: 'help', v: 'version' , p: 'projectroot', k: 'keep' },
	boolean: ['keep']
});

if(cli.input.length === 0) {
	console.log(chalk.yellow("No input files given!\nUse crunch-css -h to get usage information."));
}

var htmlroot = cli.flags.projectroot ? cli.flags.projectroot : pathWrapper.htmlroot(cli.input);
crunchCss(cli.input, {htmlroot: htmlroot, keep: cli.flags.keep}, function(results) {
	results.forEach(function(result) {
		var output = [
			chalk.bold(result.filename),
		];
		var srcFileSize = result.src.reduce(function(previousValue, currentValue) {
			return previousValue + fileSize(currentValue);
		}, 0);
		result.src.forEach(function(srcFile) {
			output.push(chalk.gray("  --- "+srcFile+" ("+humanSize(fileSize(srcFile))+")"));
		});
		var destFileSize = fileSize(result.dest);
		var savedPercentage = 1-(destFileSize/srcFileSize);
		var requestCount = result.src.length - 1;
		var requestOutput = requestCount > 1 ? "requests" : "request";
		output.push(
			chalk.green("  +++ "+shorten(result.dest, htmlroot)+" ("+humanSize(destFileSize)+"). " +
						"Saved "+percentage(savedPercentage)+" in size and " +
						requestCount+" "+requestOutput+".")
		);
		console.log(output.join('\n'));
	});
});

var fileSize = function(file) {
	return fs.statSync(wrap(file)).size;
};

