var assert = require('assert');
var crunchCss = require('../../');
var PathWrapper = require('../../lib/pathwrapper'),
    pathWrapper = new PathWrapper(process.cwd());

describe('end-to-end', function () {
    it('should generate a smaller css file if at least one css rule is not used', function (done) {
        var input = ["./test/e2e/test-fixture.html"];
        var htmlroot = pathWrapper.htmlroot(input);
        var options = { htmlroot: htmlroot, keep: true };
        crunchCss(input, options, function (results) {
            assert.ok(results.length === 1);
            done();
        });
    });
});
