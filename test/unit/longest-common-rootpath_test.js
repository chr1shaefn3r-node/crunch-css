var assert = require('assert');
var longestCommonRootpath = require("../../lib/longest-common-rootpath.js");

suite('longest-common-rootpath', function() {
	test('undefined results in empty string', function() {
		assert.equal(longestCommonRootpath(undefined), "");
	});
	test('empty array results in empty string', function() {
		assert.equal(longestCommonRootpath([]), "");
	});
	test('one file subdir results in full path without file', function() {
		var files = ['_dest/index.html'];
		assert.equal(longestCommonRootpath(files), "_dest/");
	});
	test('one file subsubdir results in full path without file', function() {
		var files = ['_dest/anfahrt/index.html'];
		assert.equal(longestCommonRootpath(files), "_dest/anfahrt/");
	});
	test('one file only results in full path without file', function() {
		var files = ['index.html'];
		assert.equal(longestCommonRootpath(files), "./");
	});
	test('multiple file results in longest common rootpath', function() {
		var files = ['_dest/page1/index.html', '_dest/page1/subpage1/index.html', '_dest/index.html'];
		assert.equal(longestCommonRootpath(files), "_dest/");
	});
});

