var assert = require("assert");
var PathWrapper = require('../../lib/pathwrapper.js');

suite('PathWrapper', function() {
	var pwd = '/home/User/Dev/project';
	var pathWrapper = new PathWrapper(pwd);
	suite('htmlroot', function() {
		test('subdir', function() {
			var actual = pathWrapper.htmlroot(['_dist/index.html']);
			var expected = pwd+"/_dist/";
			assert.equal(actual, expected);
		});
		test('cwd is already correct dir, uncss needs undefined', function() {
			var actual = pathWrapper.htmlroot(['index.html']);
			assert.equal(actual, undefined);
		});
	});
	suite('wrapCss', function() {
		test('absolute path in subdir', function() {
			var cssPath = '/css/first.css';
			var actual = pathWrapper.wrapCss('_dist/index.html', cssPath);
			var expected = pwd+"/_dist"+cssPath;
			assert.equal(actual, expected);
		});
		test('absolute path in subdir with htmlroot', function() {
			var cssPath = '/css/first.css';
			var actual = pathWrapper.wrapCss('_dist/index.html', cssPath, pwd+"/_dist/");
			var expected = pwd+"/_dist"+cssPath;
			assert.equal(actual, expected);
		});
	});
	suite('wrap', function() {
		test('absolute path', function() {
			var filePath = '/css/first.css';
			var expected = pwd+filePath;
			assert.equal(pathWrapper.wrap(filePath), expected);
		});
		test('already complete path', function() {
			var filePart = '/css/first.css';
			var filePath = pwd+filePart;
			var expected = pwd+filePart;
			assert.equal(pathWrapper.wrap(filePath), expected);
		});
	});
	suite('shorten', function() {
		test('absolut', function() {
			var expected = "/css/first.css";
			var actual = pathWrapper.shorten(pwd+"/_dist"+expected, pwd+"/_dist/");
			assert.equal(actual, expected);
		});
		test('relative', function() {
			var expected = "css/first.css";
			var actual = pathWrapper.shorten(pwd+"/"+expected, undefined);
			assert.equal(actual, expected);
		});
	});
});

