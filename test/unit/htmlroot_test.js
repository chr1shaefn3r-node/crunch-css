var assert = require('assert');
var htmlroot = require('../../lib/htmlroot.js');

suite('htmlroot', function() {
	test('one file', function() {
		var actual = htmlroot("/home/User/web", ["index.html"]);
		var expected = "/home/User/web/";
		assert.equal(actual, expected);
	});
	test('one file in subdir', function() {
		var actual = htmlroot("/home/User/web", ["_dist/index.html"]);
		var expected = "/home/User/web/_dist/";
		assert.equal(actual, expected);
	});
});
