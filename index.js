#! /usr/bin/env node
'use strict';
var fs = require("fs");
var debug = require('debug')('crunch-css'),
	Q = require('q'),
	uncss = require("uncss");
var FileProcessor = require("./lib/fileprocessor.js"),
	PathWrapper = require("./lib/pathwrapper.js");

module.exports = function(files, options, cb) {
	var pathWrapper = new PathWrapper(process.cwd()),
		wrapCss = pathWrapper.wrapCss;
	var uncssOptions = {
		htmlroot: options.htmlroot
	};
	var parsedFiles = files.map(function(file) {
		return parseFile(file);
	});
	var filesWithCssBlocks;
	Q.all(parsedFiles)
	.then(function(results) {
		return results.filter(function(result) {
			return result.css.length > 0;
		});
	}).then(function(results) {
		filesWithCssBlocks = results.map(function(result) {
			return result.filename;
		});
		if(filesWithCssBlocks.length <= 0) {
			throw new Error("No HTML-files with css-blocks");
		}
		return uncssThose(filesWithCssBlocks, uncssOptions);
	}).then(function(minimalCssString) {
		var processFiles = filesWithCssBlocks.map(function(file) {
			return processFile(file, options.keep);
		});
		return Q.all(processFiles)
			// nested because we need both: blocks and minimalCssString
			.then(function(blocks) {
				var results = blocks.map(function(block) {
					var css = block.css[0];
					var cssDest = wrapCss(block.filename, css.dest, options.htmlroot);
					fs.writeFileSync(cssDest, minimalCssString);
					fs.writeFileSync(block.filename, block.content);
					return {
						filename: block.filename,
						src: css.src,
						dest: cssDest
					};
				});
				cb(results);
			});
	})
	.catch(function(err) {
		debug(err);
	})
	.done();

	function parseFile(file) {
		var deferred = Q.defer();
		var result = new FileProcessor(file).parse();
		deferred.resolve(result);
		return deferred.promise;
	}
	function processFile(file, keep) {
		var deferred = Q.defer();
		var result = new FileProcessor(file).process(keep);
		deferred.resolve(result);
		return deferred.promise;
	}

	function uncssThose(files, options) {
		var deferred = Q.defer();
		uncss(files, options, function(err, output) {
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(output);
			}
		});
		return deferred.promise;
	}
};

